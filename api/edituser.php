<?php

	$id          = ( is_null($_REQUEST['idusuario'])       ? null : strtolower(trim($_REQUEST['idusuario']))   		);
	$nombre      = ( is_null($_REQUEST['nombre'])          ? null : trim($_REQUEST['nombre'])      					);
	$apellidos   = ( is_null($_REQUEST['apellidos'])       ? null : trim($_REQUEST['apellidos'])   					);
	$nick        = ( is_null($_REQUEST['nick'])            ? null : trim($_REQUEST['nick'])        					);
	$descripcion = ( is_null($_REQUEST['descripcion'])     ? null : trim($_REQUEST['descripcion']) 					);
	$nacimiento  = ( is_null($_REQUEST['fechanacimiento']) ? null : strtolower(trim($_REQUEST['fechanacimiento'])) 	);
	$telefono    = ( is_null($_REQUEST['telefono'])        ? null : strtolower(trim($_REQUEST['telefono']))    		);
	$sexo        = ( is_null($_REQUEST['sexo'])            ? null : strtolower(trim($_REQUEST['sexo']))        		);
	$pais        = ( is_null($_REQUEST['pais'])            ? null : trim($_REQUEST['pais'])        					);
    $ciudad      = ( is_null($_REQUEST['ciudad'])          ? null : trim($_REQUEST['ciudad'])      					);
	$email		 = ( is_null($_REQUEST['email'])           ? null : trim($_REQUEST['email'])      					);
    $facebook    = ( is_null($_REQUEST['facebook'])        ? null : strtolower(trim($_REQUEST['facebook']))    		);
	$twitter     = ( is_null($_REQUEST['twitter'])         ? null : strtolower(trim($_REQUEST['twitter']))     		);
	$password    = ( is_null($_REQUEST['password'])        ? null : trim($_REQUEST['password'])     				);
	
	$error["result"] = FALSE;
	$error["error"]  = "ERROR: La consulta fallo";

	if( !is_null($id)       && $id!=""    &&
	    !is_null($nick)     && $nick!=""  &&
	    !is_null($email)    && $email!=""   )
	{		
		// Import the connection data (username,password...)
		include 'api/db.php';
		
		// Open & Select DB connection
		$dbConnection = mysqli_connect($DB[0], $DB[1], $DB[2], $DB[3]);
		
		/* Check Error Connection */
		if ( mysqli_connect_errno() ){ $error["error"]  = "ERROR: " + mysqli_connect_error(); echo json_encode($error); exit(); }
		
		/* Set charset connection to utf8 */
		mysqli_set_charset($dbConnection,"utf8");
				
		$sql = "UPDATE USUARIO SET Nombre='" .$nombre. "' , Apellidos='" .$apellidos. "' , NickName='" .$nick. "' , Descripcion='" .$descripcion. "' , Telefono='" .$telefono. "' , Sexo='" .$sexo. "' , Pais='" .$pais. "' , Ciudad='" .$ciudad. "' , UrlFacebook='" .$facebook. "' , UrlTwitter='" .$twitter. "' , MailTo='" .$email. "' ";
		if( !is_null($password) && $password!="" )
			$sql .= " , Password='" .$password. "' ";
		if( !is_null($nacimiento) && $nacimiento!="" )
			$sql .= " , FechaNacimiento='" .$nacimiento. "' ";
		$sql .= " WHERE IdUsuario = " . $id . " ; ";
				
		// Exec query to DB
		try
		{ 
			mysqli_query($dbConnection, $sql); 
			$error["result"] = TRUE;
			$error["error"]  = "Usuario Actualizado sin problemas";
			echo json_encode($error);
		} catch(Exception $e){ $error["error"]  = "ERROR: " + $e; echo json_encode($error); }
		
		// Close DB connection
		mysqli_close($dbConnection);	
		
	}
	else
	{
		$error["result"] = FALSE;
		$error["error"]  = "ERROR: No has rellenado el campo idUsuario";
		echo json_encode($error); // PRINT DATA AS JSON
	}
	
	
?>