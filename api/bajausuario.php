<?php

	$id	= ( is_null($_REQUEST['idusuario']) ?  '' : trim($_REQUEST['idusuario']) );
	
	if( !is_null($id) && $id!= "" )
	{
		// Import the connection data (username,password...)
		include 'api/db.php';
		
		// Open & Select DB connection
		$dbConnection = mysqli_connect($DB[0], $DB[1], $DB[2], $DB[3]);
		
		/* Check Error Connection */
		if ( mysqli_connect_errno() ){ $error["error"]  = "ERROR: " + mysqli_connect_error(); echo json_encode($error); exit(); }
		
		/* Set charset connection to utf8 */
		mysqli_set_charset($dbConnection,"utf8");
		
		// QUERY SQL
		$sql = " UPDATE `USUARIO` SET `TipoUsuario` = 'noactivo' WHERE `USUARIO`.`IdUsuario` = " . $id;
				
		// Exec query to DB
		$result = mysqli_query($dbConnection, $sql);
			
		// Processing Events
		$data = mysqli_fetch_array($result,MYSQLI_ASSOC);

			// QUERY SQL
			$sql = " DELETE FROM `EVENTO` WHERE `IdCreador` = '".$id."' AND `FechaInicio` >= CURDATE() ";

			// Exec query to DB
			mysqli_query($dbConnection, $sql);
		
		$error["error"]  = "El usuario ha sido dado de baja.";
		echo json_encode($error); // PRINT DATA AS JSON
						
		// Close DB connection
		mysqli_close($dbConnection);		
	}
	else
	{
		$error["error"]  = "ERROR: No has rellenado todos los campos necesarios";
		echo json_encode($error); // PRINT DATA AS JSON
	}
	
?>