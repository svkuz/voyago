<?php
	header('Content-Type: text; charset=utf-8');

	$idevento  = ( is_null($_REQUEST['idevento'])  ? null : strtolower(trim($_REQUEST['idevento']))   );
	$idcreador = ( is_null($_REQUEST['idcreador']) ? null : strtolower(trim($_REQUEST['idcreador']))  );
	$idvotante = ( is_null($_REQUEST['idvotante']) ? null : strtolower(trim($_REQUEST['idvotante']))  );
	$votacion  = ( is_null($_REQUEST['votacion'])  ? null : strtolower(trim($_REQUEST['votacion']))   );

	$error = "ERROR";

	if( !is_null($idevento)  && $idevento!=""  && 
	    !is_null($idcreador) && $idcreador!="" && 
	    !is_null($idvotante) && $idvotante!="" && 
	    !is_null($votacion)  && $votacion!=""  &&
	   	$votacion >= 0       && $votacion <=5   )
	{
		// Import the connection data (username,password...)
		include 'api/db.php';

		// Open & Select DB connection
		$dbConnection = mysqli_connect($DB[0], $DB[1], $DB[2], $DB[3]);

		/* Check Error Connection */
		if ( mysqli_connect_errno() ){ /*printf( "Falló la conexión: %s\n", mysqli_connect_error() );*/ exit(); }

		/* Set charset connection to utf8 */
		mysqli_set_charset($dbConnection,"utf8");
		
		$caducado = FALSE;
		
		try
		{
			// QUERY SQL
			$sql  = " SELECT COUNT(*) AS 'count' FROM EVENTO WHERE FechaFin < CURDATE() AND IdEvento = " . $idevento;

			// Exec query to DB
			$result = mysqli_query($dbConnection, $sql);

			if( !is_null($result) &&  mysqli_num_rows($result) > 0)
			{
				$temp = mysqli_fetch_array($result,MYSQLI_ASSOC);					
				if( $temp['count'] == 0 || $temp['count'] == '0' ) $caducado = FALSE; else $caducado = TRUE;					
				mysqli_free_result($result);
			}
		} catch(Exception $e){}
		
		if( $caducado == TRUE )
		{
			try
			{
				// QUERY SQL
				$sql  = " INSERT INTO `VALORACION_CREADOR` (`IdEvento`, `IdCreador`, `IdVotante`, `Votacion`) VALUES ('".$idevento."', '".$idcreador."', '".$idvotante."', '".$votacion."'); ";

				// Exec query to DB
				$result = mysqli_query($dbConnection, $sql);

				mysqli_free_result($result);
			} catch(Exception $e){}
			
			try
			{
				// QUERY SQL
				$sql  = " UPDATE `VALORACION_CREADOR` SET `Votacion` = '".$votacion."' WHERE `IdEvento` = ".$idevento." AND `IdCreador` = ".$idcreador." AND `IdVotante` = ".$idvotante."; ";

				// Exec query to DB
				$result = mysqli_query($dbConnection, $sql);

				mysqli_free_result($result);
				
				$error = "Su votacion ha sido tenida en cuenta, Gracias.";
			} catch(Exception $e){}
			
		}
		else
		{
			$error = "ERROR: El Evento por el cual quieres realizar una votacion no ha finalizado.";
		}
		
		echo json_encode($error); // PRINT DATA AS JSON

		// Close DB connection
		mysqli_close($dbConnection);
	}
	else
	{
		if( !is_null($votacion)  && $votacion!=""  && ( $votacion >5 || $votacion <0 ) )
			$error = "ERROR: La votacion debe ser entre 0 y 5";
		else
			$error = "ERROR: No ha introducido todos los datos";
		
		echo json_encode($error);		
	}

?>