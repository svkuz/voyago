<?php
	
	// Import the connection data (username,password...)
	include 'api/db.php';
	
	// Open & Select DB connection
	$dbConnection = mysqli_connect($DB[0], $DB[1], $DB[2], $DB[3]);
	
	/* Check Error Connection */
	if ( mysqli_connect_errno() ){ /*printf( "Falló la conexión: %s\n", mysqli_connect_error() );*/ exit(); }
	
	/* Set charset connection to utf8 */
	mysqli_set_charset($dbConnection,"utf8");
	
	// QUERY SQL
	$sql  = " SELECT DISTINCT Destino, PaisDestino , COUNT(*) AS 'Cantidad' FROM EVENTO WHERE FechaInicio >= CURDATE() GROUP BY Destino ORDER BY COUNT(*) DESC , Destino LIMIT 10; ";
	// Exec query to DB
	$result = mysqli_query($dbConnection, $sql);
	
	$events; 
	// Processing Events
	try
	{
		if( !is_null($result) &&  mysqli_num_rows($result) > 0)
		{
			while ( $row = mysqli_fetch_array($result,MYSQLI_ASSOC) ){ $events[ count($events) ] = $row; }
			mysqli_free_result($result);
		}
	} catch(Exception $e){}
	
	echo json_encode($events); // PRINT DATA AS JSON
					
	// Close DB connection
	mysqli_close($dbConnection);		

?>