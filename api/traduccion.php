<?php
	header('Content-Type: text; charset=utf-8');
	$traduccion = "";

	// Import the connection data (username,password...)
	include 'api/db.php';

	// Open & Select DB connection
	$dbConnection = mysqli_connect($DB[0], $DB[1], $DB[2], $DB[3]);

	/* Check Error Connection */
	if ( mysqli_connect_errno() ){ /*printf( "Falló la conexión: %s\n", mysqli_connect_error() );*/ exit(); }

	/* Set charset connection to utf8 */
	mysqli_set_charset($dbConnection,"utf8");

	$sql = " SELECT DISTINCT Origen AS `all` , OrigenGN AS `json` from EVENTO UNION SELECT DISTINCT Destino AS `all` , DestinoGN AS `json` from EVENTO UNION SELECT DISTINCT PaisOrigen AS `all` , PaisOrigenGN AS `json` from EVENTO UNION SELECT DISTINCT PaisDestino AS `all` , PaisDestinoGN AS `json` from EVENTO ";
	
	// Exec query to DB
	$result = mysqli_query($dbConnection, $sql);

	$alldata;		
	// Processing Events
	try
	{
		if( !is_null($result) &&  mysqli_num_rows($result) > 0)
		{
			while ( $row = mysqli_fetch_array($result,MYSQLI_ASSOC) )
			{ 
				$alldata[count($alldata)] = $row; 
			}
			mysqli_free_result($resultE);
		}
	} catch(Exception $e){}

	foreach( $alldata as $data )
	{
		$line = "";		
		$json = json_decode( $data["json"] , true );	
		if( is_array($json) && count($json) > 0 )
		{
			// Seleccion de idioma, si hay español/ingles chachi, sinos el campo estandar sin traduccion
			$es = $json["es"] != '' ? $json["es"] : $data['all'];
			$en = $json["en"] != '' ? $json["en"] : $data['all'];
			
			// Convertimos los caracteres unicode u00~ en \u00~ para que puedan ser reconocidos por en navegador web.
			$es= str_replace('u00','\u00',$es);
			$en= str_replace('u00','\u00',$en);
			
			// Vamos a quitar acentos y simbolos raros en la traduccion al ingles, primero convertirmo unicode a utf8 y luego un string replace.
			$originales  = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
    		$modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
			$en = preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) {return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');}, $en);
    		$en = strtr(utf8_decode($en), utf8_decode($originales), $modificadas);			

			// La linea formateada y lista para imprimir.
			$line = "		\"".$es."\"". " : " . "\"".$en."\"". ",\n";
			
			$es = preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) {return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');}, $es);
			
			// La linea formateada y lista para imprimir.
			$line = "		\"".$es."\"". " : " . "\"".$en."\"". ",\n";
		}
		else
		{
			// Seleccion el campo estandar sin traduccion para ambos idiomas.
			$es = $data['all'];
			$en = $data['all'];
			
			// Convertimos los caracteres unicode u00~ en \u00~ para que puedan ser reconocidos por en navegador web.
			$es= str_replace('u00','\u00',$es);
			$en= str_replace('u00','\u00',$en);
			
			// Vamos a quitar acentos y simbolos raros en la traduccion al ingles, primero convertirmo unicode a utf8 y luego un string replace.
			$originales  = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
    		$modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
			$en = preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) {return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');}, $en);
    		$en = strtr(utf8_decode($en), utf8_decode($originales), $modificadas);		

			// La linea formateada y lista para imprimir (en caso de que no este vacio).
			if( trim($es)!='' &&  trim($en)!='')
				$line = "		\"".$es."\"". " : " . "\"".$en."\"". ",\n";
						
			$es = preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) {return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');}, $es);
			
			// La linea formateada y lista para imprimir (en caso de que no este vacio).
			if( trim($es)!='' &&  trim($en)!='')
				$line = "		\"".$es."\"". " : " . "\"".$en."\"". ",\n";
			
		}
		$traduccion .= $line;	
	}
/* NO TOCAR LA MIERDA DE LAS IDENTACIONES AUNQUE TE SANGREN LOS OJOS */
	$traduccion  = ' Lang.prototype.pack.en = {
	"token": {' . "\n" . $traduccion;
	$traduccion .= file_get_contents( 'api/traduccionMenus.txt' ) . "\n";
	$traduccion .= '	}
};';
/* NO TOCAR LA MIERDA DE LAS IDENTACIONES AUNQUE TE SANGREN LOS OJOS */

	echo $traduccion;
?>