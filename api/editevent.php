<?php

	$idevento	 = ( is_null($_REQUEST['idevento'])    	  ? null : trim($_REQUEST['idevento'])					);
	$descripcion = ( is_null($_REQUEST['descripcion'])    ? null : trim($_REQUEST['descripcion'])				);
	$fechainicio = ( is_null($_REQUEST['fechainicio'])    ? null : strtolower(trim($_REQUEST['fechainicio']))  	);
	$fechafin    = ( is_null($_REQUEST['fechafin'])       ? null : strtolower(trim($_REQUEST['fechafin']))     	);
	$transporte  = ( is_null($_REQUEST['transporte'])     ? null : strtolower(trim($_REQUEST['transporte']))   	);
	$hospedaje   = ( is_null($_REQUEST['hospedaje'])      ? null : strtolower(trim($_REQUEST['hospedaje']))    	);
	$plazas      = ( is_null($_REQUEST['plazas'])         ? null : strtolower(trim($_REQUEST['plazas']))       	);
	$precio      = ( is_null($_REQUEST['precio'])         ? null : strtolower(trim($_REQUEST['precio']))       	);
	$ideditor    = ( is_null($_REQUEST['ideditor'])       ? null : strtolower(trim($_REQUEST['ideditor']))    	);
	
	$error["result"] = FALSE;
	$error["error"]  = "ERROR: La consulta fallo";
	
	// Import the connection data (username,password...)
	include 'api/db.php';
	include 'api/geonames.php';

	if( !is_null($idevento)    && $idevento!="" )
	{
		//GET TIPO USUARIO (AGENCIA, USUARIO, PREMIUM, ADMIN...)
		$evento;
		$tipousuario;
		{
			// Open & Select DB connection
			$dbConnection = mysqli_connect($DB[0], $DB[1], $DB[2], $DB[3]);

			/* Check Error Connection */
			if ( mysqli_connect_errno() ){ $error["error"]  = "ERROR: " + mysqli_connect_error(); echo json_encode($error); exit(); }

			/* Set charset connection to utf8 */
			mysqli_set_charset($dbConnection,"utf8");

			// QUERY SQL
			$sql = " SELECT E.* , U.TipoUsuario FROM EVENTO AS E, USUARIO AS U WHERE E.IdCreador = U.IdUsuario AND E.IdEvento = " . $idevento;

			// Exec query to DB
			$result = mysqli_query($dbConnection, $sql);

			try
			{
				if( !is_null($result) &&  mysqli_num_rows($result) > 0)
				{
					$evento = mysqli_fetch_array($result,MYSQLI_ASSOC);
					$tipousuario = $evento['TipoUsuario'];
					mysqli_free_result($result);
				}
			} catch(Exception $e){}

			// Close DB connection
			mysqli_close($dbConnection);		
		}

		if( $evento['IdCreador'] == $ideditor )
		{
			// USUARIO = Agencia
			if(	!is_null($tipousuario) && $tipousuario!="" && $tipousuario=="agencia" )
			{
				if( !is_null($idevento)    && $idevento!=""    && 
					!is_null($descripcion) && $descripcion!="" && 
					!is_null($fechainicio) && $fechainicio!="" &&
					!is_null($fechafin)    && $fechafin!=""    &&
					!is_null($transporte)  && $transporte!=""  &&
					!is_null($hospedaje)   && $hospedaje!=""   &&
					!is_null($plazas)      && $plazas!=""      &&
					!is_null($precio)      && $precio!=""       )
				{		
					// Open & Select DB connection
					$dbConnection = mysqli_connect($DB[0], $DB[1], $DB[2], $DB[3]);

					/* Check Error Connection */
					if ( mysqli_connect_errno() ){$error["error"]  = "ERROR: " + mysqli_connect_error(); echo json_encode($error); exit();}

					/* Set charset connection to utf8 */
					mysqli_set_charset($dbConnection,"utf8");

					// QUERY SQL
					$sql = "UPDATE EVENTO SET Descripcion='" .$descripcion. "' , FechaInicio='" .$fechainicio. "' ,  FechaFin='" .$fechafin. "' , Transporte='" .$transporte. "' ,  Hospedaje='" .$hospedaje. "' ,  PlazasDisponibles='" .$plazas. "' ";
					if( !is_null($precio) && $precio!="" )
						$sql .= " ,  Precio='" .$precio. "' ";				
					$sql .= " WHERE IdEvento = " . $idevento . " ; ";

					// Exec query to DB
					try
					{ 
						mysqli_query($dbConnection, $sql); 
						$error["result"] = TRUE;
						$error["error"]  = "Evento de Agencia Actualizado sin problemas";
						echo json_encode($error);
					} catch(Exception $e){ $error["error"]  = "ERROR: " + $e; echo json_encode($error); }

					// Close DB connection
					mysqli_close($dbConnection);		
				}
				else
				{
					$error["result"] = FALSE;
					$error["error"]  = "ERROR: No has rellenado todos los campos necesarios" ."\n\n". "TipoUsuario:" .$tipousuario;
					echo json_encode($error); // PRINT DATA AS JSON
				}
			}
			// USUARIO != Agencia
			else
			{
				if( !is_null($idevento)    && $idevento!=""    && 
					!is_null($descripcion) && $descripcion!="" && 
					!is_null($fechainicio) && $fechainicio!="" &&
					!is_null($fechafin)    && $fechafin!=""    &&
					!is_null($transporte)  && $transporte!=""  &&
					!is_null($hospedaje)   && $hospedaje!=""   &&
					!is_null($plazas)      && $plazas!=""       )
				{			
					// Open & Select DB connection
					$dbConnection = mysqli_connect($DB[0], $DB[1], $DB[2], $DB[3]);

					/* Check Error Connection */
					if ( mysqli_connect_errno() ){ $error["error"]  = "ERROR: " + mysqli_connect_error(); echo json_encode($error); exit(); }

					/* Set charset connection to utf8 */
					mysqli_set_charset($dbConnection,"utf8");

					// QUERY SQL
					$sql = "UPDATE EVENTO SET Descripcion='" .$descripcion. "' , FechaInicio='" .$fechainicio. "' ,  FechaFin='" .$fechafin. "' , Transporte='" .$transporte. "' ,  Hospedaje='" .$hospedaje. "' ,  PlazasDisponibles='" .$plazas. "' ";
					if( !is_null($precio) && $precio!="" )
						$sql .= " ,  Precio='" .$precio. "' ";				
					$sql .= " WHERE IdEvento = " . $idevento . " ; ";

					// Exec query to DB
					try
					{ 
						mysqli_query($dbConnection, $sql); 
						$error["result"] = TRUE;
						$error["error"]  = "Evento de Usuario Actualizado sin problemas";
						echo json_encode($error);
					} catch(Exception $e){ $error["error"]  = "ERROR: " + $e; echo json_encode($error); }

					// Close DB connection
					mysqli_close($dbConnection);		
				}
				else
				{
					$error["result"] = FALSE;
					$error["error"]  = "ERROR: No has rellenado todos los campos necesarios" ."\n\n". $sql ."\n\n". "TipoUsuario:" .$tipousuario;
					echo json_encode($error); // PRINT DATA AS JSON
				}
			}
		}
		else
		{
				$error["result"] = FALSE;
				$error["error"]  = "ERROR: No Eres el creador del evento, no puedes editarlo.";
				echo json_encode($error); // PRINT DATA AS JSON
		}
	}
	else
	{
		$error["result"] = FALSE;
		$error["error"]  = "ERROR: No has introducido el campo idevento";
		echo json_encode($error); // PRINT DATA AS JSON
	}
	
?>