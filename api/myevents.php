<?php

	$idusuario = ( is_null($_REQUEST['idusuario']) ? null : strtolower(trim($_REQUEST['idusuario'])) );
	
	if(!is_null($idusuario) && $idusuario!="" )
	{
		// Import the connection data (username,password...)
		include 'api/db.php';
		
		// Open & Select DB connection
		$dbConnection = mysqli_connect($DB[0], $DB[1], $DB[2], $DB[3]);
		
		/* Check Error Connection */
		if ( mysqli_connect_errno() ){ /*printf( "Falló la conexión: %s\n", mysqli_connect_error() );*/ exit(); }
		
		/* Set charset connection to utf8 */
		mysqli_set_charset($dbConnection,"utf8");
		
		/* Set charset connection to utf8 */
		mysqli_set_charset($dbConnection,"utf8");
		
		// SQL (Consultas SQL)
		
		$sql1 = " SELECT E.IdEvento , E.Descripcion , E.ModalidadViaje AS 'Tipo', E.FechaInicio, E.FechaFin, E.Transporte, E.Hospedaje, E.Origen, E.Destino, E.PaisDestino, E.PlazasDisponibles, E.IdCreador , E.Recomendado, E.ModalidadViaje, U.TipoUsuario, U.Nombre, U.Apellidos, U.IdUsuario , C.Nombre , C.Apellidos FROM `USUARIO` AS `U` , `USUARIO` AS `C` , `EVENTO` AS `E` WHERE E.IdCreador = U.IdUsuario AND E.IdCreador = C.IdUsuario AND E.IdCreador = ".$idusuario." AND E.FechaFin >= CURDATE() ORDER BY E.FechaFin ASC ";
		
		$sql2 = " SELECT E.IdEvento , E.Descripcion , E.ModalidadViaje AS 'Tipo', E.FechaInicio, E.FechaFin, E.Transporte, E.Hospedaje, E.Origen, E.Destino, E.PaisDestino, E.PlazasDisponibles, E.IdCreador , E.Recomendado, E.ModalidadViaje, U.TipoUsuario, U.Nombre, U.Apellidos, U.IdUsuario, C.Nombre , C.Apellidos FROM `USUARIO` AS `U` , `EVENTO` AS `E` , `PARTICIPA_EVENTO` AS `P` , `USUARIO` AS `C` WHERE C.IdUsuario = E.IdCreador AND U.IdUsuario = P.IdUsuario AND P.IdEvento = E.IdEvento AND U.IdUsuario = ".$idusuario." AND E.FechaFin >= CURDATE() ORDER BY E.FechaFin ASC ";
		
		$sql3 = " SELECT E.IdEvento , E.Descripcion , E.ModalidadViaje AS 'Tipo', E.FechaInicio, E.FechaFin, E.Transporte, E.Hospedaje, E.Origen, E.Destino, E.PaisDestino, E.PlazasDisponibles, E.IdCreador , E.Recomendado, E.ModalidadViaje, U.TipoUsuario, U.Nombre, U.Apellidos, U.IdUsuario, C.Nombre , C.Apellidos FROM `USUARIO` AS `U` , `EVENTO` AS `E` , `PARTICIPA_EVENTO` AS `P` , `USUARIO` AS `C` WHERE C.IdUsuario = E.IdCreador AND U.IdUsuario = P.IdUsuario AND P.IdEvento = E.IdEvento AND U.IdUsuario = ".$idusuario." AND E.FechaFin < CURDATE() ORDER BY E.FechaFin ASC ";

		// QUERYS (Resultados)
		$query1 = mysqli_query($dbConnection, $sql1);
		$query2 = mysqli_query($dbConnection, $sql2);
		$query3 = mysqli_query($dbConnection, $sql3);	
		
		// Processing data to JSON
		$json["creados"]   = [];
		$json["participo"] = [];
		$json["historico"] = [];
		
		// QUERY 1: Creados por Mi
		try
		{
			if( !is_null($query1) &&  mysqli_num_rows($query1) > 0)
			{
				$temp1;
				while ( $row = mysqli_fetch_array($query1,MYSQLI_ASSOC) ){ $temp1[ count($temp1) ] = $row; }
				$json["creados"] = $temp1;
				mysqli_free_result($query1);
			}
		} catch(Exception $e){}
		
		// QUERY 2: Participo
		try
		{
			if( !is_null($query2) &&  mysqli_num_rows($query2) > 0)
			{
				$temp2;
				while ( $row = mysqli_fetch_array($query2,MYSQLI_ASSOC) ){ $temp2[ count($temp2) ] = $row; }
				$json["participo"] = $temp2;
				mysqli_free_result($query2);
			}
		} catch(Exception $e){}

		// QUERY 3: Historico de Participacion				
		try
		{
			if( !is_null($query3) &&  mysqli_num_rows($query3) > 0)
			{
				$temp3;
				while ( $row = mysqli_fetch_array($query3,MYSQLI_ASSOC) ){ $temp3[ count($temp3) ] = $row; }
				$json["historico"] = $temp3;
				mysqli_free_result($query3);
			}
		} catch(Exception $e){}		
		
		echo json_encode($json); // PRINT DATA AS JSON
		
		// Close DB connection
		mysqli_close($dbConnection);		
	}
	
?>