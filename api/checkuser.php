<?php
	$id = ( is_null($_REQUEST['idusuario']) ? null : strtolower(trim($_REQUEST['idusuario'])) );

	if( !is_null($id) && $id!="" )
	{
		// Import the connection data (username,password...)
		include 'api/db.php';

		// Open & Select DB connection
		$dbConnection = mysqli_connect($DB[0], $DB[1], $DB[2], $DB[3]);

		/* Check Error Connection */
		if ( mysqli_connect_errno() ){ /*printf( "Falló la conexión: %s\n", mysqli_connect_error() );*/ exit(); }

		/* Set charset connection to utf8 */
		mysqli_set_charset($dbConnection,"utf8");

		// QUERY SQL
		$sql  = " SELECT * FROM USUARIO AS U WHERE U.IdUsuario = " . $id;
		
		// Exec query to DB
		$result = mysqli_query($dbConnection, $sql);

		$user;
		// Processing Events
		try
		{
			if( !is_null($result) &&  mysqli_num_rows($result) > 0)
			{
				$user = mysqli_fetch_array($result,MYSQLI_ASSOC);
				mysqli_free_result($result);
			}
		} catch(Exception $e){}
		
		$check = FALSE;
		
		if( !is_null($user['TipoUsuario']) && $user['TipoUsuario'] == "agencia" && 
			!is_null($user['Nombre']) && $user['Nombre']!="" && 
		    !is_null($user['NickName']) && $user['NickName']!="" &&
		    !is_null($user['MailTo']) && $user['MailTo']!="" &&
		    !is_null($user['Descripcion']) && $user['Descripcion']!="" &&
		    !is_null($user['Pais']) && $user['Pais']!="" &&
		    !is_null($user['Ciudad']) && $user['Ciudad']!="" )
			$check = TRUE;
		else if ( !is_null($user['TipoUsuario']) && $user['TipoUsuario'] != "agencia" && $user['TipoUsuario'] != "noactivo" && 
			!is_null($user['Nombre']) && $user['Nombre']!="" && 
		    !is_null($user['NickName']) && $user['NickName']!="" &&
		    !is_null($user['MailTo']) && $user['MailTo']!="" &&
		    !is_null($user['Descripcion']) && $user['Descripcion']!="" &&
		    !is_null($user['Pais']) && $user['Pais']!="" &&
		    !is_null($user['Ciudad']) && $user['Ciudad']!="" &&
			!is_null($user['Apellidos']) && $user['Apellidos']!="" &&
			!is_null($user['Sexo']) && $user['Sexo']!="" &&
			!is_null($user['FechaNacimiento']) && $user['FechaNacimiento']!="" )
			$check = TRUE;

		echo json_encode($check); // PRINT DATA AS JSON

		// Close DB connection
		mysqli_close($dbConnection);
	}
	else
	{
		$check = FALSE;
		echo json_encode($check);		
	}
	
?>