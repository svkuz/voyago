<?php

	$descripcion = ( is_null($_REQUEST['descripcion'])    ? null : trim($_REQUEST['descripcion'])				);
	$fechainicio = ( is_null($_REQUEST['fechainicio'])    ? null : strtolower(trim($_REQUEST['fechainicio']))  	);
	$fechafin    = ( is_null($_REQUEST['fechafin'])       ? null : strtolower(trim($_REQUEST['fechafin']))     	);
	$transporte  = ( is_null($_REQUEST['transporte'])     ? null : strtolower(trim($_REQUEST['transporte']))   	);
	$hospedaje   = ( is_null($_REQUEST['hospedaje'])      ? null : strtolower(trim($_REQUEST['hospedaje']))    	);
	$origen      = ( is_null($_REQUEST['origen'])         ? null : trim($_REQUEST['origen'])       				);
	$paisorigen  = ( is_null($_REQUEST['paisorigen'])     ? null : trim($_REQUEST['paisorigen'])         		);
	$destino     = ( is_null($_REQUEST['destino'])        ? null : trim($_REQUEST['destino'])      				);
	$paisdestino = ( is_null($_REQUEST['paisdestino'])    ? null : trim($_REQUEST['paisdestino'])         		);
	$modalidad   = ( is_null($_REQUEST['modalidad'])      ? null : strtolower(trim($_REQUEST['modalidad']))    	);
	$plazas      = ( is_null($_REQUEST['plazas'])         ? null : strtolower(trim($_REQUEST['plazas']))       	);
	$precio      = ( is_null($_REQUEST['precio'])         ? null : strtolower(trim($_REQUEST['precio']))       	);
	$longitud    = ( is_null($_REQUEST['longitud'])       ? null : strtolower(trim($_REQUEST['longitud']))     	);
	$latitud     = ( is_null($_REQUEST['latitud'])        ? null : strtolower(trim($_REQUEST['latitud']))      	);
	$idcreador   = ( is_null($_REQUEST['idcreador'])      ? null : strtolower(trim($_REQUEST['idcreador']))    	);
	
    $idpartfechas      = ( is_null($_REQUEST['idpartfechas'])      ? $idcreador : strtolower(trim($_REQUEST['idpartfechas']))      );
	$idparthospedaje   = ( is_null($_REQUEST['idparthospedaje'])   ? $idcreador : strtolower(trim($_REQUEST['idparthospedaje']))   );
    $idparttransporte  = ( is_null($_REQUEST['idparttransporte'])  ? $idcreador : strtolower(trim($_REQUEST['idparttransporte']))  );
	
	$error["result"] = FALSE;
	$error["error"]  = "ERROR: La consulta fallo";
	
	// Import the connection data (username,password...)
	include 'api/db.php';
	include 'api/geonames.php';

	//GET TIPO USUARIO (AGENCIA, USUARIO, PREMIUM, ADMIN...)
	$tipousuario;
	{
		// Open & Select DB connection
		$dbConnection = mysqli_connect($DB[0], $DB[1], $DB[2], $DB[3]);
		
		/* Check Error Connection */
		if ( mysqli_connect_errno() ){ $error["error"]  = "ERROR: " + mysqli_connect_error(); echo json_encode($error); exit(); }
				
		/* Set charset connection to utf8 */
		mysqli_set_charset($dbConnection,"utf8");
		
		// QUERY SQL
		$sql  = " SELECT TipoUsuario FROM USUARIO WHERE IdUsuario = ".$idcreador;
		
		// Exec query to DB
		$result = mysqli_query($dbConnection, $sql);
		
		try
		{
			if( !is_null($result) &&  mysqli_num_rows($result) > 0)
			{
				$row = mysqli_fetch_array($result,MYSQLI_ASSOC);
				$tipousuario = $row['TipoUsuario'];
				mysqli_free_result($result);
			}
		} catch(Exception $e){}
		
		// Close DB connection
		mysqli_close($dbConnection);		
	}

	// USUARIO = Agencia
	if(	!is_null($tipousuario) && $tipousuario!="" && $tipousuario=="agencia" )
	{
		if(
			!is_null($descripcion) && $descripcion!="" && 
			!is_null($fechainicio) && $fechainicio!="" &&
			!is_null($fechafin)    && $fechafin!=""    &&
			!is_null($transporte)  && $transporte!=""  &&
			!is_null($hospedaje)   && $hospedaje!=""   &&
			!is_null($origen)      && $origen!=""      &&
			!is_null($paisorigen)  && $paisorigen!=""  &&
			!is_null($destino)     && $destino!=""     &&
			!is_null($paisdestino) && $paisdestino!="" &&
			!is_null($modalidad)   && $modalidad!=""   &&
			!is_null($plazas)      && $plazas!=""      &&
			!is_null($precio)      && $precio!=""      &&
			!is_null($longitud)    && $longitud!=""    &&
			!is_null($latitud)     && $latitud!=""     &&
			!is_null($idcreador)   && $idcreador!=""    )
		{		
			// Open & Select DB connection
			$dbConnection = mysqli_connect($DB[0], $DB[1], $DB[2], $DB[3]);
			
			/* Check Error Connection */
			if ( mysqli_connect_errno() ){ $error["error"]  = "ERROR: " + mysqli_connect_error(); echo json_encode($error); exit(); }
					
			/* Set charset connection to utf8 */
			mysqli_set_charset($dbConnection,"utf8");
			
			// QUERY SQL
			$sql  = " INSERT INTO `voyago`.`EVENTO` (`IdEvento`, `Descripcion`, `FechaInicio`, `FechaFin`, `Transporte`, `Hospedaje`, `Origen`, `PaisOrigen`, `Destino`, `PaisDestino`, `ModalidadViaje`, `PlazasDisponibles`, `Precio`, `IdCreador`, `IdPartFechas`, `IdPartHospedaje`, `IdPartTransporte` , `Longitud` , `Latitud` , `PaisOrigenGN` , `PaisDestinoGN` , `OrigenGN`, `DestinoGN`) ";
			$sql .= " VALUES (NULL, '".$descripcion."', '".$fechainicio."', '".$fechafin."', '".$transporte."', '".$hospedaje."', '".$origen."', '".$paisorigen."', '".$destino."', '".$paisdestino."', '".$modalidad."', '".$plazas."', '".$precio."', '".$idcreador."', '".$idpartfechas."', '".$idparthospedaje."', '".$idparttransporte."' , '".$longitud."' , '".$latitud."' , '".geonamesJson($paisorigen)."' , '".geonamesJson($paisdestino)."' , '".geonamesJson($origen)."' , '".geonamesJson($destino)."' ); ";
			
			// Exec query to DB
			try
			{ 
				mysqli_query($dbConnection, $sql); 
				$error["result"] = TRUE;
				$error["error"]  = "Evento Insertado sin problemas";
				echo json_encode($error);
			} catch(Exception $e){ $error["error"]  = "ERROR: " + $e; echo json_encode($error); }
			
			// Close DB connection
			mysqli_close($dbConnection);		
		}
		else
		{
			// QUERY SQL
			$sql  = " INSERT INTO `voyago`.`EVENTO` (`IdEvento`, `Descripcion`, `FechaInicio`, `FechaFin`, `Transporte`, `Hospedaje`, `Origen`, `PaisOrigen`, `Destino`, `PaisDestino`, `ModalidadViaje`, `PlazasDisponibles`, `Precio`, `IdCreador`, `IdPartFechas`, `IdPartHospedaje`, `IdPartTransporte` , `Longitud` , `Latitud` , `PaisOrigenGN` , `PaisDestinoGN` , `OrigenGN`, `DestinoGN`) ";
			$sql .= " VALUES (NULL, '".$descripcion."', '".$fechainicio."', '".$fechafin."', '".$transporte."', '".$hospedaje."', '".$origen."', '".$paisorigen."', '".$destino."', '".$paisdestino."', '".$modalidad."', '".$plazas."', '".$precio."', '".$idcreador."', '".$idpartfechas."', '".$idparthospedaje."', '".$idparttransporte."' , '".$longitud."' , '".$latitud."' , '".geonamesJson($paisorigen)."' , '".geonamesJson($paisdestino)."' , '".geonamesJson($origen)."' , '".geonamesJson($destino)."' ); ";
			
			$error["result"] = FALSE;
			$error["error"]  = "ERROR: No has rellenado todos los campos necesarios" ."\n\n". $sql ."\n\n". "TipoUsuario:" .$tipousuario;
			echo json_encode($error); // PRINT DATA AS JSON
		}
	}
	// USUARIO != Agencia
	else
	{
			if(
			!is_null($descripcion) && $descripcion!="" && 
			!is_null($fechainicio) && $fechainicio!="" &&
			!is_null($fechafin)    && $fechafin!=""    &&
			!is_null($origen)      && $origen!=""      &&
			!is_null($paisorigen)  && $paisorigen!=""  &&
			!is_null($destino)     && $destino!=""     &&
			!is_null($paisdestino) && $paisdestino!="" &&
			!is_null($modalidad)   && $modalidad!=""   &&
			!is_null($plazas)      && $plazas!=""      &&
			!is_null($longitud)    && $longitud!=""    &&
			!is_null($latitud)     && $latitud!=""     &&
			!is_null($idcreador)   && $idcreador!=""    )
		{		
			// Open & Select DB connection
			$dbConnection = mysqli_connect($DB[0], $DB[1], $DB[2], $DB[3]);
			
			/* Check Error Connection */
			if ( mysqli_connect_errno() ){ $error["error"]  = "ERROR: " + mysqli_connect_error(); echo json_encode($error); exit(); }
					
			/* Set charset connection to utf8 */
			mysqli_set_charset($dbConnection,"utf8");
			
			// QUERY SQL
			$sql  = " INSERT INTO `voyago`.`EVENTO` (`IdEvento`, `Descripcion`, `FechaInicio`, `FechaFin`, `Transporte`, `Hospedaje`, `Origen`, `PaisOrigen`, `Destino`, `PaisDestino`, `ModalidadViaje`, `PlazasDisponibles`, `IdCreador`, `IdPartFechas`, `IdPartHospedaje`, `IdPartTransporte` , `Longitud` , `Latitud` , `PaisOrigenGN` , `PaisDestinoGN` , `OrigenGN`, `DestinoGN`) ";
			$sql .= " VALUES (NULL, '".$descripcion."', '".$fechainicio."', '".$fechafin."', '".$transporte."', '".$hospedaje."', '".$origen."', '".$paisorigen."', '".$destino."', '".$paisdestino."', '".$modalidad."','".$plazas."', '".$idcreador."', '".$idpartfechas."', '".$idparthospedaje."', '".$idparttransporte."' , '".$longitud."' , '".$latitud."' , '".geonamesJson($paisorigen)."' , '".geonamesJson($paisdestino)."' , '".geonamesJson($origen)."' , '".geonamesJson($destino)."' ); ";
			
			// Exec query to DB
			try
			{ 
				mysqli_query($dbConnection, $sql); 
				$error["result"] = TRUE;
				$error["error"]  = "Evento Insertado sin problemas";
				echo json_encode($error);
			} catch(Exception $e){ $error["error"]  = "ERROR: " + $e; echo json_encode($error); }
			
			// Close DB connection
			mysqli_close($dbConnection);		
		}
		else
		{
			// QUERY SQL
			$sql  = " INSERT INTO `voyago`.`EVENTO` (`IdEvento`, `Descripcion`, `FechaInicio`, `FechaFin`, `Transporte`, `Hospedaje`, `Origen`, `PaisOrigen`, `Destino`, `PaisDestino`, `ModalidadViaje`, `PlazasDisponibles`, `IdCreador`, `IdPartFechas`, `IdPartHospedaje`, `IdPartTransporte` , `Longitud` , `Latitud` , `PaisOrigenGN` , `PaisDestinoGN` , `OrigenGN`, `DestinoGN`) ";
			$sql .= " VALUES (NULL, '".$descripcion."', '".$fechainicio."', '".$fechafin."', '".$transporte."', '".$hospedaje."', '".$origen."', '".$paisorigen."', '".$destino."', '".$paisdestino."', '".$modalidad."','".$plazas."', '".$idcreador."', '".$idpartfechas."', '".$idparthospedaje."', '".$idparttransporte."' , '".$longitud."' , '".$latitud."' , '".geonamesJson($paisorigen)."' , '".geonamesJson($paisdestino)."' , '".geonamesJson($origen)."' , '".geonamesJson($destino)."' ); ";
			
			$error["result"] = FALSE;
			$error["error"]  = "ERROR: No has rellenado todos los campos necesarios" ."\n\n". $sql ."\n\n". "TipoUsuario:" .$tipousuario;
			echo json_encode($error); // PRINT DATA AS JSON
		}
	}
	
?>