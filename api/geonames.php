<?php
	header('Content-Type: text; charset=utf-8');

	function geonamesJson($text)
	{
		$search = urlencode($text);
		$url = 'http://api.geonames.org/search?q='.$search.'&maxRows=1&style=full&username=voyago&type=json';
		$file = file_get_contents($url);		
		$json = json_decode($file,true)['geonames'][0]['alternateNames'];

		$alt;
		foreach ($json as $item)
		{
			$alt[$item['lang']] = $item['name'];
		}
		$json = json_encode($alt);
		
		if( is_null($json) || $json == null || $json == "null" ) $json = "";
		
		$json=str_replace("'","",$json);
		
		return $json;
	}
?>