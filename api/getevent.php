<?php
	header('Content-Type: text; charset=utf-8');

	$id 		= ( is_null($_REQUEST['idevento'])  ? null : strtolower(trim($_REQUEST['idevento']))   );
	$idusuario 	= ( is_null($_REQUEST['idusuario']) ? null : strtolower(trim($_REQUEST['idusuario']))  );

	if( !is_null($id) && $id!="" )
	{
		// Import the connection data (username,password...)
		include 'api/db.php';

		// Open & Select DB connection
		$dbConnection = mysqli_connect($DB[0], $DB[1], $DB[2], $DB[3]);

		/* Check Error Connection */
		if ( mysqli_connect_errno() ){ /*printf( "Falló la conexión: %s\n", mysqli_connect_error() );*/ exit(); }

		/* Set charset connection to utf8 */
		mysqli_set_charset($dbConnection,"utf8");

		// QUERY SQL
		$sql  = " SELECT E.* , U.IdUsuario, U.Nombre, U.Apellidos , U.NickName , U.TipoUsuario , U.Descripcion as 'DescripcionUsuario' FROM EVENTO AS E, USUARIO AS U WHERE E.IdCreador = U.IdUsuario AND E.IdEvento = " . $id;
		
		// Exec query to DB
		$result = mysqli_query($dbConnection, $sql);

		$event; 
		// Processing Events
		try
		{
			if( !is_null($result) &&  mysqli_num_rows($result) > 0)
			{
				$event = mysqli_fetch_array($result,MYSQLI_ASSOC);				
				unset($event['OrigenGN']);
				unset($event['DestinoGN']);
				unset($event['PaisOrigenGN']);
				unset($event['PaisDestinoGN']);				
				mysqli_free_result($result);
			}
		} catch(Exception $e){}
		
		// QUERY SQL
		$sql  = " SELECT U.IdUsuario , U.Nombre , U.Apellidos , U.NickName , U.Descripcion FROM PARTICIPA_EVENTO AS P , USUARIO AS U WHERE P.IdUsuario = U.IdUsuario AND P.IdEvento = " . $id;
		
		// Exec query to DB
		$result = mysqli_query($dbConnection, $sql);

		$participa; 
		// Processing Events
		try
		{
			if( !is_null($result) &&  mysqli_num_rows($result) > 0)
			{
				while ( $row = mysqli_fetch_array($result,MYSQLI_ASSOC) ){ $participa[ count($participa) ] = $row; }
				mysqli_free_result($result);
			}
		} catch(Exception $e){}	
		$event['PlazasOcupadas'] = count($participa);
		$event['Participantes'] = $participa;
		
		$event["Suscripcion"] = FALSE;
		if( !is_null($idusuario) && $idusuario!="" )
		{
			// QUERY SQL
			$sql  = " SELECT DISTINCT IdUsuario FROM PARTICIPA_EVENTO WHERE IdEvento = " . $id;

			// Exec query to DB
			$result = mysqli_query($dbConnection, $sql);

			$suscritos; 
			// Processing Events
			try
			{
				if( !is_null($result) &&  mysqli_num_rows($result) > 0)
				{
					while ( $row = mysqli_fetch_array($result,MYSQLI_ASSOC) ){ $suscritos[ count($suscritos) ] = $row; }
					mysqli_free_result($result);
				}
			} catch(Exception $e){}	
			
			for($i=0; $i<count($suscritos); $i++)
				if( $suscritos[$i]["IdUsuario"] == $idusuario )
					$event["Suscripcion"] = TRUE;			
		}		
		
		echo json_encode($event); // PRINT DATA AS JSON

		// Close DB connection
		mysqli_close($dbConnection);
	}

?>