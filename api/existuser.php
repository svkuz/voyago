<?php
	
	header('Access-Control-Allow-Origin: *');
	$email = ( is_null($_REQUEST['email']) ? null : trim($_REQUEST['email'])   );
	$nick  = ( is_null($_REQUEST['nick'])  ? null : trim($_REQUEST['nick'])    );

	// TRUE = EXISTE/OCUPADO , FALSE = LIBRE , ERROR = No hay valores para email o nick.
	$r['email'] = 'ERROR';
	$r['nick']  = 'ERROR';

	if( (!is_null($email) && $email != '') || (!is_null($nick) && $nick != '') )
	{
		// Import the connection data (username,password...)
		include 'api/db.php';

		// Open & Select DB connection
		$dbConnection = mysqli_connect($DB[0], $DB[1], $DB[2], $DB[3]);

		/* Check Error Connection */
		if ( mysqli_connect_errno() ){ /*printf( "Falló la conexión: %s\n", mysqli_connect_error() );*/ exit(); }

		/* Set charset connection to utf8 */
		mysqli_set_charset($dbConnection,"utf8");

		if(!is_null($email) && $email != '')
		{
			// QUERY SQL
			$sql  = " SELECT Count(*) as 'count' FROM `USUARIO` Where MailTo = '".$email."' ";

			// Exec query to DB
			$result = mysqli_query($dbConnection, $sql);
			$row = mysqli_fetch_array($result,MYSQLI_ASSOC);

			// TRUE = EXISTE/OCUPADO ,FALSE = LIBRE
			if($row['count'] == 0)
				$r['email'] = FALSE;
			else
				$r['email'] = TRUE;	
		}

		if(!is_null($nick) && $nick != '')
		{
			// QUERY SQL
			$sql  = " SELECT Count(*) as 'count' FROM `USUARIO` Where Nickname = '".$nick."' ";

			// Exec query to DB
			$result = mysqli_query($dbConnection, $sql);
			$row = mysqli_fetch_array($result,MYSQLI_ASSOC);

			// TRUE = EXISTE/OCUPADO ,FALSE = LIBRE
			if($row['count'] == 0)
				$r['nick'] = FALSE;
			else
				$r['nick'] = TRUE;	
		}

		echo json_encode($r); // PRINT DATA AS JSON

		// Close DB connection
		mysqli_close($dbConnection);
	}

?>