<?php

	$origen  = ( is_null($_REQUEST['origen'])  ? null  : strtolower(trim($_REQUEST['origen']))  );
	$destino = ( is_null($_REQUEST['destino']) ? null  : strtolower(trim($_REQUEST['destino'])) );
	$inicio  = ( is_null($_REQUEST['inicio'])  ? null  : strtolower(trim($_REQUEST['inicio']))  );
	$fin     = ( is_null($_REQUEST['fin'])     ? null  : strtolower(trim($_REQUEST['fin']))     );
	$agencia = ( is_null($_REQUEST['agencia']) ? null  : strtolower(trim($_REQUEST['agencia'])) );
	$tipo    = ( is_null($_REQUEST['tipo'])    ? null  : strtolower(trim($_REQUEST['tipo']))  );
	$idusuario = ( is_null($_REQUEST['idusuario']) ? 0 : strtolower(trim($_REQUEST['idusuario'])) );
		
	if( !is_null($destino) && $destino!="" )
	{
		// Import the connection data (username,password...)
		include 'api/db.php';
		
		// Open & Select DB connection
		$dbConnection = mysqli_connect($DB[0], $DB[1], $DB[2], $DB[3]);
		
		/* Check Error Connection */
		if ( mysqli_connect_errno() ){ /*printf( "Falló la conexión: %s\n", mysqli_connect_error() );*/ exit(); }
				
		/* Set charset connection to utf8 */
		mysqli_set_charset($dbConnection,"utf8");
		
		// QUERY SQL
		$sqlE  = " SELECT E.* , U.* , (SELECT COUNT(*) FROM `PARTICIPA_EVENTO` AS `P` WHERE P.IdEvento = E.IdEvento) AS 'PlazasOcupadas' FROM `USUARIO` AS `U` , `EVENTO` AS `E` ";
		$sqlE .= " WHERE U.IdUsuario = E.IdCreador ";
		//$sqlE .= " AND E.Origen LIKE '%".$origen."%' AND E.Destino LIKE '%".$destino."%' ";
		$sqlE .= " AND ( E.DestinoGN LIKE '%:\"".$destino."\"%' OR E.Destino LIKE '%".$destino."%' ) ";
		if( !is_null($origen) && $origen!="" )
			$sqlE .= " AND ( E.OrigenGN LIKE '%:\"".$origen."\"%' OR E.Origen LIKE '%".$origen."%' ) ";
		if( !is_null($inicio) && $inicio!="" ) 
			$sqlE .= " AND E.FechaInicio >= '".$inicio."' ";
		if( !is_null($fin) && $fin!="" ) 
			$sqlE .= " AND E.FechaFin <= '".$fin."' ";
		if( !is_null($tipo) && $tipo!="" ) 
			$sqlE .= " AND E.ModalidadViaje LIKE '%".$tipo."%' ";
		if( !is_null($agencia) && $agencia!="" && $agencia == 'true' ) 
			$sqlE .= " AND U.TipoUsuario = 'agencia' ";
		//else if( !is_null($agencia) && $agencia!="" && $agencia == 'false' ) 
		//	$sqlE .= " AND U.TipoUsuario != 'agencia' ";
		$sqlE .= " AND E.FechaInicio >= CURDATE() ORDER BY E.FechaFin; ";
		$sqlU = " SELECT E.IdEvento FROM `USUARIO` AS `U` , `EVENTO` AS `E` , `PARTICIPA_EVENTO` AS `P` WHERE U.IdUsuario = P.IdUsuario AND P.IdEvento = E.IdEvento AND U.IdUsuario = ".$idusuario." AND E.FechaInicio >= CURDATE() ORDER BY E.FechaFin ASC";
				
		// Exec query to DB
		$resultE = mysqli_query($dbConnection, $sqlE);
		$resultU = mysqli_query($dbConnection, $sqlU);
		
		$events; $user;	
		
		// Processing Events
		try
		{
			if( !is_null($resultE) &&  mysqli_num_rows($resultE) > 0)
			{
				/*
				while ( $row = mysqli_fetch_array($resultE,MYSQLI_ASSOC) )
				{ 
					$row["Suscripcion"]   = FALSE;
					$row["PaisOrigenGN"]  = json_decode($row["PaisOrigenGN"]);
					$row["PaisDestinoGN"] = json_decode($row["PaisDestinoGN"]);
					$row["OrigenGN"]      = json_decode($row["OrigenGN"]);
					$row["DestinoGN"]     = json_decode($row["DestinoGN"]);
					$events[ count($events) ] = $row; 
				}
				*/
				while ( $row = mysqli_fetch_array($resultE,MYSQLI_ASSOC) )
				{ 
					$row["Suscripcion"]   = FALSE;
					$row["PaisOrigenGN"]  = is_array( json_decode($row["PaisOrigenGN"] ,true)  )  ? json_decode($row["PaisOrigenGN"])  : array() ;
					$row["PaisDestinoGN"] = is_array( json_decode($row["PaisDestinoGN"],true)  )  ? json_decode($row["PaisDestinoGN"]) : array() ;
					$row["OrigenGN"]      = is_array( json_decode($row["OrigenGN"],true)       )  ? json_decode($row["OrigenGN"])      : array() ;
					$row["PaisDestinoGN"] = is_array( json_decode($row["DestinoGN"],true)      )  ? json_decode($row["DestinoGN"])     : array() ;
					$events[ count($events) ] = $row; 
				}
				mysqli_free_result($resultE);
			}
		} catch(Exception $e){}
		
		// Processing User
		if( !is_null($idusuario) && $idusuario != 0 )
		{
			try
			{
				if( !is_null($resultU) &&  mysqli_num_rows($resultU) > 0)
				{
					while ( $row = mysqli_fetch_array($resultU,MYSQLI_ASSOC) ){ $user[ count($user) ] = $row; }
					mysqli_free_result($resultU);
				}
			} catch(Exception $e){}
			
			for($i=0; $i<count($events); $i++)
				for($j=0; $j<count($user); $j++)
					if( $events[$i]["IdEvento"] == $user[$j]["IdEvento"] )
					{
						$events[$i]["Suscripcion"] = TRUE;
					}
		}
		
		echo json_encode($events); // PRINT DATA AS JSON
		
		// Close DB connection
		mysqli_close($dbConnection);		
	}
	
?>