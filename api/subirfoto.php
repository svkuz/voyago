<?php

	$id     = (  is_null($_REQUEST['idusuario']) ? null : strtolower(trim($_REQUEST['idusuario']))  );
	$avatar = (  is_null($_REQUEST['avatar'])    ? null : strtolower(trim($_REQUEST['avatar']))     );

	$error["result"] = FALSE;
	$error["error"]  = "ERROR: La consulta fallo";

	if( !is_null($id) && $id!="" )
	{
		$error["error"]  = "Error: No se ha enviado fichero";
		if( !is_null($_FILES['avatar']['tmp_name']) )
		{
			$error["error"]  = "Error: El Tipo no es image/jpeg";
			if ($_FILES['avatar']['type'] == "image/jpeg")
			{
				$error["error"]  = "Error: Tamaño muy grande";
				if ($_FILES['avatar']['size'] <= 2000000)
				{
					$error["result"] = TRUE;
					$error["error"]  = "Foto Subida";
					rename("images/profile/".$id.".jpg","images/profile/".$id.".backup");
					if( !copy($_FILES['avatar']['tmp_name'], "images/profile/".$id.".jpg") )
						rename("images/profile/".$id.".backup","images/profile/".$id.".jpg");
					else
						unlink("images/profile/".$id.".backup");
				}
			}
		}
		echo json_encode($error); // PRINT DATA AS JSON		
	}
	else
	{
		$error["result"] = FALSE;
		$error["error"]  = "ERROR: No has rellenado el campo idUsuario";
		echo json_encode($error); // PRINT DATA AS JSON
	}
	
	
?>