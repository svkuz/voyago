<?php

	$text      = '%';
	$idusuario = ( is_null($_REQUEST['idusuario']) ? 0    : strtolower(trim($_REQUEST['idusuario'])) );
	
	if(!is_null($text) && $text!="" )
	{
		// Import the connection data (username,password...)
		include 'api/db.php';
		
		// Open & Select DB connection
		$dbConnection = mysqli_connect($DB[0], $DB[1], $DB[2], $DB[3]);
		
		/* Check Error Connection */
		if ( mysqli_connect_errno() ){ /*printf( "Falló la conexión: %s\n", mysqli_connect_error() );*/ exit(); }
		
		/* Set charset connection to utf8 */
		mysqli_set_charset($dbConnection,"utf8");
		
		// QUERY SQL
		//$sqlE  = " SELECT E.* , U.*  FROM `USUARIO` AS `U` , `EVENTO` AS `E` WHERE U.IdUsuario = E.IdCreador  AND  ( E.Descripcion LIKE '%".$text."%' OR E.Destino LIKE '%".$text."%' ) AND E.FechaInicio >= CURDATE() ORDER BY E.FechaFin ASC ";
		$sqlE = " SELECT DISTINCT E.* , U.*, (SELECT COUNT(*) FROM `PARTICIPA_EVENTO` AS `P` WHERE P.IdEvento = E.IdEvento) AS 'PlazasOcupadas' FROM `USUARIO` AS `U` , `EVENTO` AS `E` WHERE U.IdUsuario = E.IdCreador AND ( E.Descripcion LIKE '%".$text."%' OR E.Destino LIKE '%".$text."%' ) AND E.FechaInicio >= CURDATE() ORDER BY E.FechaFin ASC ";
		
		$sqlU = " SELECT E.IdEvento FROM `USUARIO` AS `U` , `EVENTO` AS `E` , `PARTICIPA_EVENTO` AS `P` WHERE U.IdUsuario = P.IdUsuario AND P.IdEvento = E.IdEvento AND U.IdUsuario = ".$idusuario." AND E.FechaInicio >= CURDATE() ORDER BY E.FechaFin ASC";
		
		// Exec query to DB
		$resultE = mysqli_query($dbConnection, $sqlE);
		$resultU = mysqli_query($dbConnection, $sqlU);
		
		$events; $user;	
		
		// Processing Events
		try
		{
			if( !is_null($resultE) &&  mysqli_num_rows($resultE) > 0)
			{
				while ( $row = mysqli_fetch_array($resultE,MYSQLI_ASSOC) )
				{ 
					$row["Suscripcion"]   = FALSE;
					$row["PaisOrigenGN"]  = is_array( json_decode($row["PaisOrigenGN"] ,true)  )  ? json_decode($row["PaisOrigenGN"])  : array() ;
					$row["PaisDestinoGN"] = is_array( json_decode($row["PaisDestinoGN"],true)  )  ? json_decode($row["PaisDestinoGN"]) : array() ;
					$row["OrigenGN"]      = is_array( json_decode($row["OrigenGN"],true)       )  ? json_decode($row["OrigenGN"])      : array() ;
					$row["PaisDestinoGN"] = is_array( json_decode($row["DestinoGN"],true)      )  ? json_decode($row["DestinoGN"])     : array() ;
					$events[ count($events) ] = $row; 
				}
				mysqli_free_result($resultE);
			}
		} catch(Exception $e){}
		
		// Processing User
		if( !is_null($idusuario) && $idusuario != 0 )
		{
			try
			{
				if( !is_null($resultU) &&  mysqli_num_rows($resultU) > 0)
				{
					while ( $row = mysqli_fetch_array($resultU,MYSQLI_ASSOC) ){ $user[ count($user) ] = $row; }
					mysqli_free_result($resultU);
				}
			} catch(Exception $e){}
			
			for($i=0; $i<count($events); $i++)
				for($j=0; $j<count($user); $j++)
					if( $events[$i]["IdEvento"] == $user[$j]["IdEvento"] )
					{
						$events[$i]["Suscripcion"] = TRUE;
					}
		}
		
		echo json_encode($events); // PRINT DATA AS JSON
						
		// Close DB connection
		mysqli_close($dbConnection);		
	}

?>