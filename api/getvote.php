<?php

	$idevento	 = ( is_null($_REQUEST['idevento'])   ? null : trim($_REQUEST['idevento'])  );
	$idusuario   = ( is_null($_REQUEST['idusuario'])  ? null : trim($_REQUEST['idusuario']) );
	$idcreador = -1;

	if( !is_null($idevento) && $idevento!="" && !is_null($idusuario) && $idusuario!="")
	{
		// Import Database
		include 'api/db.php';
		
		// Open & Select DB connection
		$dbConnection = mysqli_connect($DB[0], $DB[1], $DB[2], $DB[3]);

		/* Check Error Connection */
		if ( mysqli_connect_errno() ){ $error["error"]  = "ERROR: " + mysqli_connect_error(); echo json_encode($error); exit(); }

		/* Set charset connection to utf8 */
		mysqli_set_charset($dbConnection,"utf8");

		// QUERY SQL
		$sql = " SELECT IdCreador FROM EVENTO WHERE IdEvento = " . $idevento;

		// Exec query to DB
		$result = mysqli_query($dbConnection, $sql);

		try
		{
			if( !is_null($result) &&  mysqli_num_rows($result) > 0)
			{
				$evento = mysqli_fetch_array($result,MYSQLI_ASSOC);
				$idcreador = $evento['IdCreador'];
				mysqli_free_result($result);
			}
		} catch(Exception $e){}
		
		// QUERY SQL
		$sql = " SELECT Votacion , COUNT(*) as 'count' FROM `VALORACION_CREADOR` WHERE IdVotante = '".$idusuario."' AND IdEvento = '".$idevento."' AND IdCreador = '".$idcreador."' ";

		// Exec query to DB
		$result = mysqli_query($dbConnection, $sql);

		$event;
		try
		{
			if( !is_null($result) &&  mysqli_num_rows($result) > 0)
			{
				$event = mysqli_fetch_array($result,MYSQLI_ASSOC);
				mysqli_free_result($result);
			}
		} catch(Exception $e){}		

		// Close DB connection
		mysqli_close($dbConnection);
		
		echo json_encode($event);
	}
	else
	{
		echo json_encode("Error: No has introducido todo los datos necesarios.");
	}

?>