<?php

	$text = "%";
	
	if(!is_null($text) && $text!="" )
	{
		// Import the connection data (username,password...)
		include 'api/db.php';
		
		// Open & Select DB connection
		$dbConnection = mysqli_connect($DB[0], $DB[1], $DB[2], $DB[3]);
		
		/* Check Error Connection */
		if ( mysqli_connect_errno() ){ /*printf( "Falló la conexión: %s\n", mysqli_connect_error() );*/ exit(); }
		
		// QUERY SQL
		$sql = " SELECT E.IdEvento , E.Descripcion ,  E.ModalidadViaje AS 'Tipo', E.FechaInicio, E.FechaFin, E.Transporte, E.Hospedaje, E.Origen, E.Destino, E.PaisDestino, E.PlazasDisponibles, E.IdCreador, E.Recomendado , U.TipoUsuario, U.Nombre, U.Apellidos, U.Avatar FROM `USUARIO` AS `U` , `EVENTO` AS `E` WHERE U.IdUsuario = E.IdCreador AND ( E.Descripcion LIKE '%".$text."%' OR E.Destino LIKE '%".$text."%' ) AND E.FechaFin >= CURDATE() ORDER BY E.FechaFin ASC ";
		
		// Exec query to DB
		$result = mysqli_query($dbConnection, $sql);
		
		// Processing data to JSON
		try
		{
			if( !is_null($result) &&  mysqli_num_rows($result) > 0)
			{
				$json;
				while ( $row = mysqli_fetch_array($result,MYSQLI_ASSOC) ){ $json[ count($json) ] = $row; }
				mysqli_free_result($result);
				echo json_encode($json); // PRINT DATA AS JSON
			}
		} catch(Exception $e){}
		
		// Close DB connection
		mysqli_close($dbConnection);		
	}
	
?>