<?php 	$id = (  is_null($_REQUEST['idusuario']) ? null : strtolower(trim($_REQUEST['idusuario']))  ); ?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>
	<title>Cambie su foto de perfil</title>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
	<!-- production -->
	<script type="text/javascript" src="js/plupload.full.min.js"></script>
	<link href="style.css" rel="stylesheet" type="text/css"/>
</head>
<body style="background: #eee; color: #333">
	<div style="margin: 20px;">
		<h1 style="margin-bottom: 20px;">Cambie su foto de perfil</h1>

		<img src="images/profile/<?php echo $id; ?>.jpg" onerror="this.src='images/profile/default-user.jpg'" width="200" height="200"> <br/> <br/>

		<div id="filelist">Your browser doesn't have Flash, Silverlight or HTML5 support.</div>  <br/>

		<div id="container">
			<a id="pickfiles" href="javascript:;">Seleccione la imagen de perfil</a> 
			<a id="uploadfiles" href="javascript:;" lang="es">Subir</a>
		</div>

		<br />
		<pre id="console"></pre>
	</div>

	<script type="text/javascript">
	// Custom example logic

	var uploader = new plupload.Uploader({
		runtimes : 'html5,flash,silverlight,html4',
		browse_button : 'pickfiles', // you can pass an id...
		container: document.getElementById('container'), // ... or DOM Element itself
		url : 'upload.php?idusuario=<?php echo $id; ?>',
		flash_swf_url : 'Moxie.swf',
		silverlight_xap_url : 'Moxie.xap',

		filters : {
			max_file_size : '10mb',
			mime_types: [
				{title : "Image files", extensions : "jpg,gif,png"},
				{title : "Zip files", extensions : "zip"}
			]
		},

		init: {
			PostInit: function() {
				document.getElementById('filelist').innerHTML = '';

				document.getElementById('uploadfiles').onclick = function() {
					uploader.start();
					return false;
				};
			},

			FilesAdded: function(up, files) {
				plupload.each(files, function(file) {
					document.getElementById('filelist').innerHTML += '<div style="margin-bottom: 20px;" id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
				});
			},

			UploadProgress: function(up, file) {
				document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
			},
			UploadComplete: function(up, files) {
				// Called when all files are either uploaded or failed
				alert("Su imagen de perfil ha sido cambiada correctamente");
                window.close();
			},
			Error: function(up, err) {
				document.getElementById('console').appendChild(document.createTextNode("\nError #" + err.code + ": " + err.message));
			}
		}
	});

	uploader.init();

	</script>
	
</body>
</html>
