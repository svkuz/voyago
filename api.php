<?php
	$action = ( is_null($_REQUEST['action']) ? null : strtolower(trim($_REQUEST['action'])) );
	
	switch ($action)
	{
		case "search":	
			include 'api/search.php';
			break;

		case "textsearch":	
			include 'api/textsearch.php';
			break;
			
		case "allevents":	
			include 'api/allevents.php';
			break;
			
		case "myevents":	
			include 'api/myevents.php';
			break;
		
		case "createevent":	
			include 'api/createevent.php';
			break;
			
		case "getevent":	
			include 'api/getevent.php';
			break;
		
		case "editevent":	
			include 'api/editevent.php';
			break;	
			
		case "subscription":	
			include 'api/subscription.php';
			break;

		case "top10c":	
			include 'api/top10c.php';
			break;
			
		case "top10u":	
			include 'api/top10u.php';
			break;
			
		case "top10a":	
			include 'api/top10a.php';
			break;
			
		case "alld":	
			include 'api/alld.php';
			break;

		case "existuser":	
			include 'api/existuser.php';
			break;
			
		case "createuser":	
			include 'api/createuser.php';
			break;
			
		case "login":	
			include 'api/login.php';
			break;
			
		case "edituser":	
			include 'api/edituser.php';
			break;
			
		case "baja":	
			include 'api/bajausuario.php';
			break;
			
		case "getuser":	
			include 'api/getuser.php';
			break;
			
		case "getuseroldevents":	
			include 'api/getuseroldevents.php';
			break;
			
		case "map":	
			include 'api/map.php';
			break;		
			
		case "translate":	
			include 'api/traduccion.php';
			break;	
						
		case "vote":	
			include 'api/valorar.php';
			break;	
			
		case "getvote":	
			include 'api/getvote.php';
			break;	
			
		case "checkuser":	
			include 'api/checkuser.php';
			break;	
			
		case "api-help":
			echo $action; //include 'api/api-help.php';
			break;
			
		case "subir":
			include 'api/subirfoto.php';
			break;
		
		default:
			echo "[ERROR: ".$action."]";
			break;
	}
?>