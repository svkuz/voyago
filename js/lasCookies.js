/*jslint browser: true*/
/*global $, jQuery, alert*/
var lang = new Lang('es');
Lang.prototype.attrList.push('data-hint');
$.ajax({
    url: "menuBar.html",
    success: function (data) {
        $(data).prependTo('body');
        //alert("Hay cookies?)) "+getCookie("idioma"));

        if (getCookie("idioma") === ";;;") {
            setCookie("idioma", "es", 50);
            $("#cambiadorImagenIdioma").attr("src", "images/flags/uniking.png");
        } else {
            if (getCookie("idioma") === "en") {
                $("#cambiadorImagenIdioma").attr("src", "images/flags/spain.png");
                window.lang.change('en');
            } else {
                $("#cambiadorImagenIdioma").attr("src", "images/flags/uniking.png");
                //window.lang.change('es');
            }
        }

        if (getCookie("usuarioID") !== ";;;" && getCookie("usuarioID") !== "-1") {
            $("#botonCrearEventoMenu").show();
            $("#myEventsMenu").show();
            $("#imagenPerfilMenu").show();
            $("#registroMenu").hide();
            $("#imgPerfilMenu").attr("src", "images/profile/" + getCookie("usuarioID") + ".jpg");
            $("#perfilMenu").attr("href", "perfil.html#" + getCookie("usuarioID"));
            $("#imgPerfilMenu").attr("onerror", "this.src= \'images/profile/default-user.jpg\';");
            if (getCookie("tipoUsuario") === "agencia") {
                $("#formaBotonCrearEvento").attr("action", "crearEventoAgencia.html");
            } else {
                $("#formaBotonCrearEvento").attr("action", "crearEvento.html");
            }
        } else {
            setCookie("usuarioID", "-1", 50);
            setCookie("tipoUsuario", "-1", 50);
            setCookie("nomApe", "-1", 50);
        }

    },
    dataType: 'html'
});

function botonCerrarSesion() {
    console.log("Cookies de usuario cambiadas a -1");
    setCookie("usuarioID", "-1", 50);
    setCookie("tipoUsuario", "-1", 50);
    setCookie("nomApe", "-1", 50);
    $("#botonCrearEventoMenu").hide();
    $("#myEventsMenu").hide();
    $("#imagenPerfilMenu").hide();
    $("#registroMenu").show();
    //location.reload();
    window.location.href = "index.html";
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return ";;;";
}

function checkCookie(cname, valor) {
    var user = getCookie(cname);
    if (user != "") {
        console.log("Hay cookie");
    } else {
        //user = prompt("Please enter your name:", "");
        if (user != "" && user != null) {
            setCookie(cname, valor, 40);
        }
    }
}

function eliminarCookie(cname) {
    if (getCookie(cname) === ";;;") {
        console.log("No se ha encontrado la cookie al eliminar");
    } else {
        document.cookie = cname + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
    }
}

function cambiarImagenIdioma(element) {
    console.log("Idioma pulsado");
    var right = "images/flags/spain.png";
    var left = "images/flags/uniking.png";
    console.log("el bln es: " + element.bln + " y el src es: " + element.src);
    // element.src = element.bln ? right : left;
    // element.bln = !element.bln;
    if (element.src.indexOf("spain") > -1) {
        //alert("Está en español");
        element.src = left;
        console.log("Está en inglés");
        window.lang.change('es');
        setCookie("idioma", "es", 50);
    } else {
        //alert("Está en inglés");
        element.src = right;
        console.log("Está en español");
        window.lang.change('en');
        setCookie("idioma", "en", 50);
    }
}