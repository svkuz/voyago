<?php

// This script is used when a event is updated.

/** The parameters passed to this script should be:
 *  1. The affected event
 *  2. List of users subscribed to the event
 * (3.) Description of update, or what has changed, for example time, price, etc.
 *  See http://crimago.upv.edu.es/voyago/pruebaSueco.html for a dummy example
 */
$event = $_GET["event"];

//Type of update. Can be one or more of:
//Descripcion, Transporte or Hospedaje
$update = $_GET["update"];

//ES or EN.
$language = $_GET["language"];

if (($update != "Descripcion" && $update != "Transporte" && $update != "Hospedaje") || is_null($event) || is_null($language)){
	echo "Invalid input!";
	}else{

//Users affeceted
$users = getEventUserEmails($event);

//Send it to Pär aswell
$users[] = "paer.linder@gmail.com";
//$sql  = "SELECT FechaInicio, FechaFin, Destino, PaisDestino, PlazasDisponibles
//Variables that have to be retrieved from the event

$mysql = getEventInfo($event);
$fechaInicio = $mysql[0]["FechaInicio"];
$fechaFin = $mysql[0]["FechaFin"];
$fechas = $fechaInicio . ' - ' . $fechaFin;
$image = $mysql[0]["Destino"];
$destinacion = $image . ', ' . $mysql[0]["PaisDestino"];
$participantes = $mysql[0]["PlazasDisponibles"];
$creador = $mysql[0]["IdCreador"];
	
$update_info = $mysql[0][$update];

if (is_null($update_info) || $update_info == ''){
	
	if ($language == "ES"){
	$update_info = "El campo está vacio!";
	}
	
	if ($language == "EN"){
	$update_info = "The creator has not filled in any information yet!";
	}
}
	
//Idiomas
//$htmlmail = getEmail($language);

switch($language){
	case "ES":
		if ($update == "Descripcion"){
			$update = 'Descripción';
			$articulo = 'la';
			$appendn = '';
			$appends = 'a';
		}else {
			$articulo = 'el';
			$appendn = '';
			$appends = 'o';
		}
		
		$titulo = $update .' del evento actualizad'. $appends . '!';
		$h2 = 'El creador del evento ha modificado ' . $articulo . ' ' . $update . ' del viaje a ' . $destinacion . '...';
		$encab ='...y es importante que te asegures de que hayas notado los cambios.';
		$mitad ='Te has subscrito a un viaje a ' . $destinacion . '. Por alguna razón, el creador del evento ha actualizado '. $articulo . ' ' . 		$update . ' del evento. Te aconsejamos que visites el evento en Voyago y que notes los cambios. Este puede ser el viaje de tu vida y no quieres sorpresas desagradables!';
		$boton ='Visitar evento!';
		$pie ='Este mensaje te ha sido enviado al estar suscrito a un evento de Voyago. Si no te has subscrito al evento, por favor ignora este correo o envíanos un correo electronico.';
		$pais ='España';
		break;
		
	case "EN":
		switch($update){
			case "Descripcion":
				$update = "description";
				break;
			case "Transporte":
				$update = "transport";
				break;
			case "Hospedaje":
				$update = "lodging";
				break;
		}
		$titulo ='Event ' . $update . ' updated!';
		$h2 = 'The creator of the event has changed the ' . $update . ' of the journey to ' . $image . '... ';
		$encab ='...and it is important that you make sure that you have noticed the changes!';
		$mitad ='You have enrolled in an event to ' . $image . '. For some reason, the creator of the event has made some changes in the '.$update.' of that event. We advice you to visit the event at Voyago and that you note the changes. This might be the journey of your life, and you would not want any unpleasant surprises!';
		$boton ='Visit event!';
		$pie =  'This message has been sent to you because you have enrolled in an event at Voyago. If you have not enrolled in an event, please ignore this message or send us an e-mail.';
		$pais = 'Spain';
	break;
}

	
	
//The email sent
//actualizar con el link al evento ("visitar evento!")
$htmlmail = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml">&#13;
 &#13;
<head>&#13;
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />&#13;
  <title>Voyago</title>&#13;
  &#13;
</head>&#13;
&#13;
<body yahoo="" bgcolor="#f6f8f1" style="min-width: 100% !important; margin: 0; padding: 0;">&#13;
<table width="100%" bgcolor="#f6f8f1" border="0" cellpadding="0" cellspacing="0">&#13;
<tr>&#13;
  <td>&#13;
    <!--[if (gte mso 9)|(IE)]>
      <table width="600" align="center" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td>
    <![endif]-->     &#13;
    <table bgcolor="#ffffff" class="content" align="center" cellpadding="0" cellspacing="0" border="0" style="width: 100%; max-width: 600px;">&#13;
      <tr>&#13;
        <td bgcolor="#EB6841" class="header" style="padding: 40px 30px 20px;">&#13;
          <table width="70" align="left" border="0" cellpadding="0" cellspacing="0">  &#13;
            <tr>&#13;
              <td height="70" style="padding: 0 20px 20px 0;">&#13;
                <img class="fix" src="images/voyago100.png" width="70" height="70" border="0" alt="" style="height: auto;" />&#13;
              </td>&#13;
            </tr>&#13;
          </table>&#13;
          <!--[if (gte mso 9)|(IE)]>
            <table width="425" align="left" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td>
          <![endif]-->&#13;
          <table class="col425" align="left" border="0" cellpadding="0" cellspacing="0" style="width: 100%; max-width: 425px;">  &#13;
            <tr>&#13;
              <td height="70">&#13;
                <table width="100%" border="0" cellspacing="0" cellpadding="0">&#13;
                  <tr>&#13;
                    <td class="subhead" style="font-size: 15px; color: #ffffff; font-family: sans-serif; letter-spacing: 10px; padding: 0 0 0 3px;">&#13;
                      VOYAGO&#13;
                    </td>&#13;
                  </tr>&#13;
                  <tr>&#13;
                    <td class="h1" style="color: #ffffff; font-size: 33px; line-height: 38px; font-weight: bold; font-family: sans-serif; padding: 5px 0 0;">&#13;
                      ' . $titulo . '&#13;
                    </td>&#13;
                  </tr>&#13;
                </table>&#13;
              </td>&#13;
            </tr>&#13;
          </table>&#13;
          <!--[if (gte mso 9)|(IE)]>
                </td>
              </tr>
          </table>
          <![endif]-->&#13;
        </td>&#13;
      </tr>&#13;
      <tr>&#13;
        <td class="innerpadding borderbottom" style="border-bottom-width: 1px; border-bottom-color: #f2eeed; border-bottom-style: solid; padding: 30px;">&#13;
          <table width="100%" border="0" cellspacing="0" cellpadding="0">&#13;
            <tr>&#13;
              <td class="h2" style="color: #153643; font-family: sans-serif; font-size: 24px; line-height: 28px; font-weight: bold; padding: 0 0 15px;">&#13;
                ' . $h2 . '&#13;
              </td>&#13;
            </tr>&#13;
            <tr>&#13;
              <td class="bodycopy" style="color: #153643; font-family: sans-serif; font-size: 16px; line-height: 22px;">&#13;
                ' . $encab . '&#13;
              </td>&#13;
            </tr>&#13;
          </table>&#13;
        </td>&#13;
      </tr>&#13;
      <tr>&#13;
        <td class="innerpadding borderbottom" style="border-bottom-width: 1px; border-bottom-color: #f2eeed; border-bottom-style: solid; padding: 30px;">&#13;
          <table width="115" align="left" border="0" cellpadding="0" cellspacing="0">  &#13;
            <tr>&#13;
              <td height="115" style="padding: 0 20px 20px 0;">&#13;
				<div style="padding: 20px; border: 1px solid #00a0b0;">&#13;
					<img class="event" src="images/events.min/'. $image .'.jpg" border="0" alt="" style="height: 190px; width: 280px;" />&#13;
					<div style="width: 280px; color: #153643; font-size: 18px; font-family: sans-serif; padding: 20px 0 0;">&#13;
					'. $fechas .'&#13;
					</div>&#13;
					<div style="width: 280px; color: #CC333F; font-size: 20px; font-weight: bold; font-family: sans-serif; padding: 10px 0 0;">&#13;
					' .$destinacion. '&#13;
					</div>&#13;
					<div style="width: 280px; height: 32px; color: #153643; font-size: 18px; font-family: sans-serif; display: flex; justify-content: bottom-left; padding: 10px 0 0;">&#13;
					<div style="float: left; align-self: top; padding: 0 10px 0 0;">' .$participantes. '</div><div style="align-self: top;"><img style="width: 32px; height: 32px;" src="images/users256r.png" /></div>&#13;
					</div>&#13;
					<div style="width: 280 px; height: 60px;">
					<table style="float: right">
						<tr>
							<td align="right">
								<table>  
									<tr>
										<td></td>
										<td rowspan=2><img src="images/profile/' . $creador . '.jpg" width = "90px" height="90px"/></td>
									</tr>  
									<tr>
										<td colspan=2><img src="images/circle_cut.png" width = "94px" height="94px"/></td>
									</tr>  
								</table>
							</td>
						</tr>
					</table>
					</div>&#13;
				</div>&#13;
              </td>&#13;
            </tr>&#13;
			
            <tr>&#13;
              <td class="h2" style="color: #153643; font-family: sans-serif; font-size: 24px; line-height: 28px; font-weight: bold; padding: 0 0 15px;">&#13;
                ' . $update . '&#13;
              </td>&#13;
            </tr>&#13;
            <tr>&#13;
              <td class="bodycopy" style="color: #153643; font-family: sans-serif; font-size: 16px; line-height: 22px;">&#13;
                ' . $update_info . '<p>&#13;
              </td>&#13;
            </tr>&#13;
			
          </table>&#13;
          <!--[if (gte mso 9)|(IE)]>
            <table width="380" align="left" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td>
          <![endif]-->&#13;
          <table class="col380" align="left" border="0" cellpadding="0" cellspacing="0" style="width: 100%; max-width: 190px;">  &#13;
            <tr>&#13;
              <td>&#13;
                <table width="100%" border="0" cellspacing="0" cellpadding="0">&#13;
                  <tr>&#13;
                    <td class="bodycopy" style="color: #153643; font-family: sans-serif; font-size: 16px; line-height: 22px;">&#13;
                      ' . $mitad . '&#13;
                    </td>&#13;
                  </tr>&#13;
                  <tr>&#13;
                    <td style="padding: 20px 0 0;">&#13;
                      <table class="buttonwrapper" bgcolor="#EB6841" border="0" cellspacing="0" cellpadding="0" style="background-color: transparent !important;">&#13;
                        <tr>&#13;
                          <td class="button" height="45" style="text-align: center; font-size: 18px; font-family: sans-serif; font-weight: bold; padding: 0px;" align="center">&#13;
                            <a href="http://crimago.upv.edu.es/voyago/evento.html#' . $event . '" style="color: #ffffff; text-decoration: none; background-color: #EB6841; padding: 15px 15px 13px;">' . $boton . '</a>&#13;
                          </td>&#13;
                        </tr>&#13;
                      </table>&#13;
                    </td>&#13;
                  </tr>&#13;
                </table>&#13;
              </td>&#13;
            </tr>&#13;
          </table>&#13;
          <!--[if (gte mso 9)|(IE)]>
                </td>
              </tr>
          </table>
          <![endif]-->&#13;
        </td>&#13;
      </tr>&#13;
      <tr>&#13;
        <td class="innerpadding bodycopy" style="color: #153643; font-family: sans-serif; font-size: 16px; line-height: 22px; padding: 30px;">&#13;
          ' . $pie . '&#13;
        </td>&#13;
      </tr>&#13;
      <tr>&#13;
        <td class="footer" bgcolor="#EB6841" style="padding: 20px 30px 15px;">&#13;
          <table width="100%" border="0" cellspacing="0" cellpadding="0">&#13;
            <tr>&#13;
              <td align="center" style="padding: 0;">&#13;
                <table border="0" cellspacing="0" cellpadding="0">&#13;
                  <tr>&#13;
                    <td width="37" style="text-align: center; padding: 0 10px;" align="center">&#13;
                      <a href="http://crimago.upv.edu.es/voyago/">&#13;
                        <img src="images/voyago100.png" width="37" height="37" alt="Voyago" border="0" style="height: auto;" />&#13;
                      </a>&#13;
                    </td>&#13;
                  </tr>&#13;
                </table>&#13;
              </td>&#13;
            </tr>&#13;
			<tr>&#13;
              <td align="center" class="footercopy" style="font-family: sans-serif; font-size: 14px; color: #ffffff; padding: 10px 0 0;">&#13;
                ® Voyago, ' . $pais . ' 2015<br />&#13;
              </td>&#13;
            </tr>&#13;
          </table>&#13;
        </td>&#13;
      </tr>&#13;
    </table>&#13;
    <!--[if (gte mso 9)|(IE)]>
          </td>
        </tr>
    </table>
    <![endif]-->&#13;
    </td>&#13;
  </tr>&#13;
</table>&#13;
</body>&#13;
</html>';

//Send the update.
sendUpdate($destinacion, $users, $htmlmail, $language);
}

//Returns a list of all the users enrolled in an event's emails
function getEventUserEmails($id){
		// Import the connection data (username,password...)
		include 'api/db.php';

		// Open & Select DB connection
		$dbConnection = mysqli_connect($DB[0], $DB[1], $DB[2], $DB[3]);

		/* Check Error Connection */
		if ( mysqli_connect_errno() ){ /*printf( "Falló la conexión: %s\n", mysqli_connect_error() );*/ exit(); }

		/* Set charset connection to utf8 */
		mysqli_set_charset($dbConnection,"utf8");

		// QUERY SQL
		//$sql  = "SELECT FechaInicio, FechaFin, Destino, PaisDestino, PlazasDisponibles FROM EVENTO WHERE EVENTO.IdEvento = $id";
		    $sql = "SELECT `USUARIO`.`MailTo` AS Email FROM `PARTICIPA_EVENTO` JOIN `USUARIO` ON `PARTICIPA_EVENTO`.`IdUsuario` = `USUARIO`.`IdUsuario` WHERE `PARTICIPA_EVENTO`.`IdEvento` = $id";
		// Exec query to DB
		$result = mysqli_query($dbConnection, $sql);

		$events; 
		// Processing Events
		try
		{
			if( !is_null($result) &&  mysqli_num_rows($result) > 0)
			{
				while ( $row = mysqli_fetch_array($result,MYSQLI_ASSOC) ){ $events[ count($events) ] = $row; }
				mysqli_free_result($result);
			}
		} catch(Exception $e){}
		
		foreach($events as $m){
			$emails[] = $m["Email"];
		}
		return $emails; // PRINT DATA AS JSON

		// Close DB connection
		mysqli_close($dbConnection);
}

//Returns some parameters about the event
function getEventInfo($id){
		// Import the connection data (username,password...)
		include 'api/db.php';

		// Open & Select DB connection
		$dbConnection = mysqli_connect($DB[0], $DB[1], $DB[2], $DB[3]);

		/* Check Error Connection */
		if ( mysqli_connect_errno() ){ /*printf( "Falló la conexión: %s\n", mysqli_connect_error() );*/ exit(); }

		/* Set charset connection to utf8 */
		mysqli_set_charset($dbConnection,"utf8");

		// QUERY SQL
		$sql  = "SELECT Descripcion, Hospedaje, Transporte, IdCreador, FechaInicio, FechaFin, Destino, PaisDestino, PlazasDisponibles FROM EVENTO WHERE EVENTO.IdEvento = $id";
		// Exec query to DB
		$result = mysqli_query($dbConnection, $sql);

		$events; 
		// Processing Events
		try
		{
			if( !is_null($result) &&  mysqli_num_rows($result) > 0)
			{
				while ( $row = mysqli_fetch_array($result,MYSQLI_ASSOC) ){ $events[ count($events) ] = $row; }
				mysqli_free_result($result);
			}
		} catch(Exception $e){}

		return $events; // PRINT DATA AS JSON

		// Close DB connection
		mysqli_close($dbConnection);
}


//Sends the email.
function sendUpdate($destinacion, $users, $htmlmail, $language)
{
    //SMTP needs accurate times, and the PHP time zone MUST be set
    //This should be done in your php.ini, but this is how to do it if you don't have access to that
    date_default_timezone_set('Etc/UTC');
    require 'PHPMailer/PHPMailerAutoload.php';
    //Create a new PHPMailer instance
    $mail = new PHPMailer;
	$mail->CharSet = "UTF-8";
    //Tell PHPMailer to use SMTP
    $mail->isSMTP();
    //Enable SMTP debugging
    // 0 = off (for production use)
    // 1 = client messages
    // 2 = client and server messages
    $mail->SMTPDebug   = 2;
    //Ask for HTML-friendly debug output
    $mail->Debugoutput = 'html';
    //Set the hostname of the mail server
    $mail->Host        = 'smtp.gmail.com';
    // use
    // $mail->Host = gethostbyname('smtp.gmail.com');
    // if your network does not support SMTP over IPv6
    //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
    $mail->Port        = 587;
    //Set the encryption system to use - ssl (deprecated) or tls
    $mail->SMTPSecure  = 'tls';
    //Whether to use SMTP authentication
    $mail->SMTPAuth    = true;
    //Username to use for SMTP authentication - use full email address for gmail
    $mail->Username    = "voyagonoreplypin@gmail.com";
    //Password to use for SMTP authentication
    $mail->Password    = "patriciopatricio";
    //Set who the message is to be sent from
    $mail->setFrom('noreply@voyago.com', 'Voyago Noreply');
    //Set an alternative reply-to address
    //$mail->addReplyTo('replyto@example.com', 'First Last');
    //Set who the message is to be sent to
    
    foreach ($users as $user) {
        if (filter_var($user, FILTER_VALIDATE_EMAIL)) {
            $mail->addBCC($user);
			//$mail->addAddress($user);
        }
        
    }
    //Set the subject line
	switch($language){
		case "ES":
			$mail->Subject = 'Viaje a ' . $destinacion . ' actualizado!';
			break;
		case "EN":
			$mail->Subject = 'Journey to ' . $destinacion . ' updated!';
			break;
	}
    //Read an HTML message body from an external file, convert referenced images to embedded,
    //convert HTML into a basic plain-text alternative body
	//$mail->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));
	$mail->msgHTML($htmlmail);
	
	//$mail->Body    = $htmlmail;
    //Replace the plain text body with one created manually
    //$mail->AltBody = 'This is a plain-text message body';
    //Attach an image file
    //$mail->addAttachment('images/phpmailer_mini.png');
    //send the message, check for errors
    if (!$mail->send()) {
        echo "Mailer Error: " . $mail->ErrorInfo;
    } else {
        echo "Message sent to: ";
        foreach ($users as $user) {
            if (filter_var($user, FILTER_VALIDATE_EMAIL)) {
                echo " " . $user;
            }
        }
    }
}
