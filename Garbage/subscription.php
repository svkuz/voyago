<?php

	$task      = ( is_null($_REQUEST['task'])      ? null : strtolower(trim($_REQUEST['task'])) );
	$idevento  = ( is_null($_REQUEST['idevento'])  ? null : strtolower(trim($_REQUEST['idevento']))  );
	$idusuario = ( is_null($_REQUEST['idusuario']) ? null : strtolower(trim($_REQUEST['idusuario'])) );

	
	if(!is_null($task) && $task!="" && !is_null($idevento) && $idevento!="" && !is_null($idusuario) && $idusuario!="")
	{
		// Import the connection data (username,password...)
		include 'api/db.php';
		
		// Open & Select DB connection
		$dbConnection = mysqli_connect($DB[0], $DB[1], $DB[2], $DB[3]);
		
		/* Check Error Connection */
		if ( mysqli_connect_errno() ){ /*printf( "Falló la conexión: %s\n", mysqli_connect_error() );*/ exit(); }
		
		// SHOW QUERY
		$sql1  = " SELECT E.IdEvento , E.Descripcion , E.FechaInicio, E.FechaFin, E.Transporte, E.Hospedaje, E.Origen, E.Destino, E.PaisDestino, E.PlazasDisponibles, E.IdCreador , E.Recomendado, E.ModalidadViaje , U.TipoUsuario, U.Nombre, U.Apellidos, U.Avatar  FROM `USUARIO` AS `U` , `EVENTO` AS `E` WHERE U.IdUsuario = E.IdCreador  AND  ( E.Descripcion LIKE '%".$text."%' OR E.Destino LIKE '%".$text."%' ) AND E.FechaFin >= CURDATE() ORDER BY E.FechaFin ASC ";
		// INSERT QUERY
		$sql2  = " INSERT INTO `u279046256_bd`.`PARTICIPA_EVENTO` (`IdEvento`, `IdUsuario`) VALUES ('".$idevento."', '".$idusuario."'); ";
		// DELETE QUERY
		$sql3  = " DELETE FROM `u279046256_bd`.`PARTICIPA_EVENTO` WHERE `PARTICIPA_EVENTO`.`IdEvento` = ".$idevento." AND `PARTICIPA_EVENTO`.`IdUsuario` = ".$idusuario." ";
		
		if( $task == 'show' )
			$sql = $sql1;
		else if( $task == 'insert' )
			$sql = $sql2;
		else if( $task == 'delete' )
			$sql = $sql3;
		else
			$sql = null;
		
		if( !is_null($sql) )
		{
			// Exec query to DB
			$result = mysqli_query($dbConnection, $sql);
	
			// Processing Events
			try
			{
				if( !is_null($result) )
				{
					$json = $result;
					//mysqli_free_result($result);
					echo json_encode($json); // PRINT DATA AS JSON
				}
			} catch(Exception $e){ echo "ERROR: " + $e; }
		}
						
		// Close DB connection
		mysqli_close($dbConnection);		
	}

?>